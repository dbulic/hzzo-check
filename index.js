#!/usr/bin/node

require("dotenv").config()

// puppeteer-extra is a drop-in replacement for puppeteer,
// it augments the installed puppeteer with plugin functionality
const puppeteer = require('puppeteer-extra')
const nodemailer = require('nodemailer');
// console.log(process.env.GMAIL_MAIL)
// process.exit()
const transporter = nodemailer.createTransport({
    host: 'smtp.gmail.com',
    port: 587,
    auth: {
      user: `${process.env.GMAIL_MAIL}`,
      pass: `${process.env.GMAIL_PASSWORD}`,
    },
});
transporter.verify().then().catch(console.error);



// add recaptcha plugin and provide it your 2captcha token (= their apiKey)
// 2captcha is the builtin solution provider but others would work as well.
// Please note: You need to add funds to your 2captcha account for this to work
const RecaptchaPlugin = require('puppeteer-extra-plugin-recaptcha')
puppeteer.use(
  RecaptchaPlugin({
    provider: {
      id: '2captcha',
      token: process.env.CAPTCHA_API_KEY // REPLACE THIS WITH YOUR OWN 2CAPTCHA API KEY ⚡
    },
    visualFeedback: false // colorize reCAPTCHAs (violet = detected, green = solved)
  })
)

// puppeteer usage as normal
puppeteer.launch({ headless: true }).then(async browser => {
    const page = await browser.newPage()
    await page.goto('http://www.hzzo-net.hr/statos_OIB.htm')

    await page.type("#upoib", process.env.OIB)

    // That's it, a single line of code to solve reCAPTCHAs 🎉
    await page.solveRecaptchas()

    await Promise.all([
        page.waitForNavigation(),
        page.click(`input[type=image]`)
    ])
    let resp = await page.evaluate(() => document.querySelectorAll('td.vazecapolica'))
    let resp2 = await page.evaluate(() => document.querySelectorAll('td.obvnaslov'))
    let mailBody = "";
    if (!resp // 👈 null and undefined check
            || Object.keys(resp).length !== 0
            || Object.getPrototypeOf(resp) !== Object.prototype) {
            
            mailBody = await page.evaluate(() => document.querySelectorAll('td.vazecapolica')[1].textContent)
    }
    else if (!resp2 // 👈 null and undefined check
        || Object.keys(resp2).length !== 0
        || Object.getPrototypeOf(resp2) !== Object.prototype) {
        
            mailBody = await page.evaluate(() => document.querySelectorAll('td.obvnaslov')[0].innerHTML)
    }
    // console.log(mailBody)

    transporter.sendMail({
        from: `"Automated Script" <${process.env.MAIL_FROM}>`, // sender address
        to: `${process.env.MAIL_TO}`, // list of receivers
        subject: "HZZO provjera osiguranja", // Subject line
        html: `<strong>${mailBody}</strong>`, // html body
    }).then(info => {
        // console.log({info});
    }).catch(console.error);

    // await page.screenshot({ path: 'response.png', fullPage: true })

    await browser.close()
})


